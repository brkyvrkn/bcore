#
# Created on Sun Jul 26 2020
#
# Copyright (c) 2020 Berkay Vurkan
#

import importlib.resources as _rsc

from configparser import ConfigParser

_cfg = ConfigParser()
with _rsc.path("core", "config.cfg") as _path:
    _cfg.read(str(_path))

# spec data
__author__ = "Berkay Vurkan"
__copyright__ = "Copyright 2020"
__credits__ = []
__license__ = "MIT"
__version__ = _cfg.get("about", "version")
__maintainer__ = "Berkay Vurkan"
__email__ = "berkayvurkan@yahoo.com"
__status__ = "Development"

URL = _cfg.get("connection", "base_url")
ENV = _cfg.get("connection", "environment")