#
# Created on Sun Jul 26 2020
#
# Copyright (c) 2020 Berkay Vurkan
#

import core

def main():
    print("Core Tool by " + core.__author__)
    pass

if __name__ == "__main__":
    main()