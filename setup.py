#
# Created on Sun Jul 26 2020
#
# Copyright (c) 2020 Berkay Vurkan
#

import os
import re
import setuptools

envstring = lambda var: os.environ.get(var) or ""

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ""

if os.path.isfile("variables.txt"):
    try:
        with open("variables.txt", "r") as fh:
            variables = fh.read().strip().split("\n")
        for v in variables:
            key, value = v.split("=")
            os.environ[key] = re.sub("['\"]", "", value)
    except:
        pass

setuptools.setup(
    license=envstring("LICENSE"),
    name=envstring("NAME"),
    version=envstring("VERSION"),
    author=envstring("AUTHOR"),
    author_email=envstring("AUTHOR_EMAIL"),
    description=envstring("DESCRIPTION"),
    url=envstring("URL"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent"
    ],
    packages=["core", "tests"],
    install_requires=["numpy", "matplotlib", "pandas"],
    python_requires='>=3.6',
    include_package_data=True,
    entry_points={"console_scripts": ["bcore=core.__main__:main"]}
)
